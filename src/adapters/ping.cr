require "./adapter"

class PingAdapter < Adapter
  @@PING_RGX = /(.+)( 0% packet loss)(.*)/
  @@PING_LATENCY_RGX = /time\=(\d+(\.\d+)?) ms/
  @@PING_ERROR_RGX = /icmp_seq\=(\d+)\ ([^time].*)$'/

  def self.query(ctx, serv_cfg)
    stdout = IO::Memory.new

    proc = Process.new(
      "ping",
      args: ["-c", "1", serv_cfg["ping_addr"]],
      output: stdout,
      error: stdout)

    status = proc.wait
    buffer = stdout.to_s

    # TODO check status code for alive check.
    alive = !@@PING_RGX.match(buffer).nil?
    latency = @@PING_LATENCY_RGX.match(buffer)

    if latency.nil?
      latency = 0.to_i64
    else
      lat_i64 = latency[1].to_i64?
      lat_f64 = latency[1].to_f64?

      if lat_i64.nil? && lat_f64.nil?
        latency = 0.to_i64
      elsif lat_i64.nil? && !lat_f64.nil?
        latency = Math.max(lat_f64, 1.to_f64)
      elsif !lat_i64.nil?
        latency = lat_i64
      else
        latency = 0.to_i64
      end
    end

    if latency.is_a?(Float64)
      latency = latency.to_i64
    end

    # At this point, latency is UInt64.

    if alive
      return AdapterResult.new(alive, latency)
    else
      # extract error data and raise
      err = @@PING_ERROR_RGX.match(buffer)

      if !alive && !err.nil?
        err = err[2]
      else
        err = "packet lost"
      end

      raise AdapterError.new(err)
    end
  end
end

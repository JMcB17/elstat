require "./manager"

def putswarn(msg)
  puts "config warning : #{msg}"
end

def ensure_alerts(cfg, alert_string, errs : Array(String), key)
  alert_string.split(',').each do |alert_id|
    if !cfg.has_key?("alert:#{alert_id}")
      errs.push("#{key}.alerts has unknown alert : '#{alert_id}'")
    end
  end
end

def ensure_keys(
  value, keys : Array(String), key : String, errs : Array(String)
)
  keys.each do |check_key|
    if !value.has_key?(check_key)
      errs.push("#{key}.#{check_key} is required")
    end
  end
end

def check_root(cfg) : Array(String)
  root = cfg["elstat"]
  errs = [] of String

  ensure_keys root, ["password", "slow_threshold_ms", "slow_threshold", "down_threshold", "order"], "elstat", errs

  incident_alerts = root["incident_alert"]?
  unless incident_alerts.nil?
    alert = root["incident_alert"]
    ensure_alerts(cfg, incident_alerts, errs, "elstat.incident_alert")
  else
    putswarn "elstat.incident_alert is recommended"
  end

  alerts = root["alerts"]?
  unless alerts.nil?
    ensure_alerts(cfg, alerts, errs, "elstat.alerts")
  end

  serv_names = root["order"].split(',')

  serv_names.each do |serv_name|
    if !cfg.has_key?("service:#{serv_name}")
      errs.push("elstat.order contains nonexisting service : #{serv_name}")
    end
  end

  return errs
end

def check_service(cfg, key, value, errs)
  ensure_keys value, ["description", "poll"], key, errs

  serv_names = cfg["elstat"]["order"].split(',')
  _, service_name = key.split(":")
  if !serv_names.includes?(service_name)
    putswarn "service '#{service_name}' is not declared in order"
  end

  if value.has_key?("adapter")
    adp_type = value["adapter"]

    case adp_type
    when "http"
      ensure_keys value, ["http_url"], key, errs
    when "ping"
      ensure_keys value, ["ping_addr"], key, errs
    else
      errs.push("#{key}.adapter is an invalid adapter type")
    end
  else
    errs.push("#{key}.adapter is required")
  end

  if value.has_key?("alerts")
    ensure_alerts(cfg, value["alerts"], errs, key)
  else
    putswarn("#{key}.alerts is recommended")
  end
end

def check_alert(cfg, key, value, errs)
  if value.has_key?("type")
    alert_type = value["type"]

    case alert_type
    when "discord"
      ensure_keys value, ["url"], key, errs
    when "email"
      ensure_keys value, ["from", "to", "host", "port", "tls_mode", "login", "password"], key, errs
    else
      errs.push("#{key}.type is an invalid alert type")
    end
  else
    errs.push("#{key}.type is required")
  end
end

# Check the given configuration for errors.
def check_config(cfg : Hash(String, Hash(String, String)))
  errs = check_root(cfg)

  cfg.each do |key, value|
    if key.starts_with?("service:")
      check_service(cfg, key, value, errs)
    elsif key.starts_with?("alert:")
      check_alert(cfg, key, value, errs)
    end
  end

  if errs.size > 0
    errs.each do |msg|
      puts "config check fail : #{msg}"
    end

    exit 1
  end
end
